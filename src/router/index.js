import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from "@/views/AboutView.vue"
import Login from "@/views/Login.vue";
import Register from "@/views/Register.vue";
import Create from "@/components/chat/Create.vue";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'fwrf',
    component: HomeView
  },
  {
    path: '/create',
    name: 'createModal',
    component: Create
  },
  {
   path: '/home/:id',
   name: 'home',
    component: AboutView
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
